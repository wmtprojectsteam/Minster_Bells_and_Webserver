#!/usr/bin/env python3
# minsterfunctions.py V0.8:
#                Wimborne Model Town
#      Minster Bells & MP3 Player Timing Software
#
#  **********************************************************************
# This program carries out the following functions:
#  1.  Triggers the playing of a recording of the Minster chimes at the hours.
#  2.  Triggers the Quarter Jack hammer at the quarters.
#  3.  Triggers the playing of a recording of the Quarter Jack bells at the quarters from the Tower.
#  4.  Provides an MP3 Player function to allow the playing of background church music in the Chancel.
#  5.  Allows the MP3 Player to be used to play change rings when required.
#  6.  Plays a Wedding Sequence of:
#        Wedding March.
#        Wedding vows.
#        Widor's Toccata and Fugue.
#        Change rings.
#  7.  User interface in the form of toggle switches.
#
# It uses the apsscheduler library to determine the key trigger points for the time related functions.
#
# It uses the RPi.GPIO Library threading capability to detect switch events.
#
# Copyright (c) 2017 Wimborne Model Town http://www.wimborne-modeltown.com/
#
#    This code is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Scheduler.py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    with this software.  If not, see <http://www.gnu.org/licenses/>.
#
#  ***********************************************************************

import datetime
import time
import os
import subprocess
import glob
from apscheduler.schedulers.background import BackgroundScheduler
import RPi.GPIO as GPIO

import logging
logging.basicConfig()

################################### Setup GPIO  ######################################

GPIO.setmode(GPIO.BCM)

GPIO.setup(20, GPIO.IN)                     # Extended Hours Enabling Pin.
GPIO.setup(21, GPIO.IN)                     # Extended Hours Disabling Pin.
GPIO.setup(22, GPIO.IN)                     # Change Rings Enabling Pin.
GPIO.setup(23, GPIO.IN)                     # Change Rings Disabling Pin.
GPIO.setup(24, GPIO.IN)                     # MP3 Player Playlist Selector Pin.
GPIO.setup(25, GPIO.IN)                     # MP3 Disabling Pin.
GPIO.setup(26, GPIO.IN)                     # Wedding Sequence Enabling Pin.
GPIO.setup(27, GPIO.IN)                     # Wedding Sequence Disabling Pin.

GPIO.setup(17, GPIO.OUT)                    # Left Solenoid (Quarter Jack arm) Actuator 
GPIO.setup(18, GPIO.OUT)                    # Right Solenoid (Quarter Jack arm) Actuator
GPIO.output(17, GPIO.LOW)                    # Initialise Left Solenoid 
GPIO.output(18, GPIO.LOW)                    # Initialise Right Solenoid


################## Setup Variables - Use globals to make configuration easy.  ############################

# Vars used in multiple places.
enable_late = False                          # Late hours not OK (will be set / unset by switch operation).
hour = 10                                    # Hour in 24 hr format
opening_hours = False                        # When set, it's during Opening Hours (during late hours). 
tower_vol = '70%'                            # Set the volume level for the chimes.
chancel_vol = '60%'                          # Set the volume level fot ther chimes.
msg_subdir = './media/Messages'              # directory containing all audible messages.

# Chimes and Quarterjack functions.    All times in seconds.
quarters_delay = 120                        # Delay after real Minster bells before quarters
hours_chime_delay = 4                       # Delay after Quarterjack strikes the las pair of the hour
quarter = 15                                # Which quarter?
chimes = 10                                 # Number of chimes to ring
quarter_rings = 1                           # Number of Quaterjack pairs to ring
chms_subdir = './media/Chimes'                    # Directory containing mp3 files for the chimes

sched = BackgroundScheduler()               # Setup the scheduler

# Change Rings control
chr_subdir = './media/Change_Rings'         # directory containing the mp3 file for the Change Rings.
c_player = None                             # mpg321 object status.  When None, not playing. When contains object ID, playing.

# MP3 Functions
# Locations for the different Playlists.
playlists = ['./media/Playlist1', './media/Playlist2', './media/Playlist3', './media/Playlist4', './media/Playlist5', './media/Playlist6']
playlist_index = 2                          # '0' means set to off by operator.  '1' to '6' means selected Playlist.  Default '2' to ensure cold start.
mp3_player = None                           # mpg321 object status.  When None, not playing. When contains object ID, playing.

# Wedding Sequence control
ws_subdir = './media/Wedding_Sequence'      # directory containing mp3 files for the Ceremony
wb_subdir = './media/Wedding_Bells'         # directory containing mp3 file for the wedding changes
wtr_player = None                           # Tower mpg321 object status.  When None, not playing. When contains object ID, playing.
wch_player = None                           # Chancel mpg321 object status.  When None, not playing. When contains object ID, playing.
ws_timer = None                             # Status of interval timer launched in Wedding Sequence.

################################################ Define Procedures  #######################################

def calculate_hours():
    global chimes

    print("In calculate_hours(). Hour = " + str(hour))
    if hour > 12:                           # Calculate the number of chimes to sound.
        chimes = hour - 12
    else:
        chimes = hour

    print(chimes)

def check_silent_hours():
    global opening_hours, hour

    now = datetime.datetime.now()
    hour = now.hour                         # Get the actual hour in 24 hr format

    print("In check_silent_hours(). Hour = " + str(hour))

    if hour >= 17:                          # Check if time after normal daytime opening
        if enable_late is False:            # If it is, make sure that Late Opening has not been set
            print("Silent Hours")
            opening_hours = False
        else:                               # Late Opening set, so ringing OK until 10.
            if hour <= 22:
                print("Late hours")
                opening_hours = True
    else:                                   # Daytime Opening hours
        if hour >= 10:
            print("Opening Hours")
            opening_hours = True
        else:
            print("Silent Hours")
            opening_hours = False

def hours():
    global chimes

    check_silent_hours()
    if opening_hours is False:
        pass                                # Do nothing
    else:                                   # Play the sequence using the delays defined above
        calculate_hours()
        print(datetime.datetime.now())
        print(str(chimes) + " O'Clock")
        fc_file = 'One_Chime.mp3'           # File for the 1st and subsequent chimes
        lc_file = 'End_Chime.mp3'           # File for the last chime
        fchimes_path = os.path.join(chms_subdir, fc_file)
        lchime_path = os.path.join(chms_subdir, lc_file)
        quarters()
        time.sleep(hours_chime_delay)
        while chimes > 1:
            print(chimes)
            subprocess.call(['mpg321', '-m', '-a', 'hw:0,0', fchimes_path])
            chimes -= 1
        subprocess.call(['mpg321', '-m', '-a', 'hw:0,0', lchime_path])
        print("Last Chime")

def calculate_quarters():
    global quarter_rings, quarter

    now = datetime.datetime.now()
    quarter = now.minute                      # Get the actual hour in 24 hr format

    if quarter == 15:                         # Calculate the number of quarterjack pairs to sound.
        quarter_rings = 1
    elif quarter == 30:
        quarter_rings = 2
    elif quarter == 45:
        quarter_rings = 3
    else:
        quarter_rings = 4

def strike_hammers():
            GPIO.output(17, GPIO.HIGH)       # Strike with Left Solenoid
            time.sleep(0.05)
            GPIO.output(17, GPIO.LOW)        # Release Left Solenoid
            time.sleep(0.5)
            GPIO.output(18, GPIO.HIGH)       # Strike with Right Solenoid
            time.sleep(0.25)
            GPIO.output(18, GPIO.LOW)        # Release Right Solenoid
            time.sleep(0.2)

def quarters():
    global quarter_rings

    check_silent_hours()
    calculate_quarters()
    if opening_hours is False:
        pass                                 # Do nothing
    else:                                    # Play the sequence using the delays defined above
        print(datetime.datetime.now())
        print("Quarter No " + str(quarter_rings))
        fq_file = 'Wimborne_One_Quarter.mp3'           # File for one Quarterjack pair of chimes
        lq_file = 'Wimborne_End_Quarter.mp3'           # File for the last Quarterjack pair of chimes
        fq_path = os.path.join(chms_subdir, fq_file)
        lq_path = os.path.join(chms_subdir, lq_file)
        time.sleep(quarters_delay)
        while quarter_rings > 1:
            print("In While Loop" + str(quarter_rings))
            print(quarter_rings)
            subprocess.Popen(['mpg321', '-m', '-a', 'hw:0,0', fq_path])
            strike_hammers()
            time.sleep(1)

            quarter_rings -= 1

        subprocess.Popen(['mpg321', '-m', '-a', 'hw:0,0', lq_path])
        strike_hammers()
        print("Last Quarter Pair")

def set_volume():
    subprocess.call(['amixer', '-c', '0', 'set', 'PCM,0', 'playback', tower_vol])
    subprocess.call(['amixer', '-c', '1', 'set', 'Speaker,0', 'playback', chancel_vol])

def extended_hours_trigger(channel):
    global enable_late
    
    msg_file = 'Late_Hours_Enabled.wav'
    msg_path = os.path.join(msg_subdir, msg_file)

    subprocess.call(['aplay', '--device=plughw:1,0', msg_path])
    time.sleep(2)

    print 'Message Played'

    print "Extended Hours Enabled.\n"
    enable_late = True
    print enable_late

def extended_hours_disable(channel):
    global enable_late
    
    print "Extended Hours Disabled\n"
    enable_late = False
    print enable_late
    
    msg_file = 'Late_Hours_Disabled.wav'
    msg_path = os.path.join(msg_subdir, msg_file)

    subprocess.call(['aplay', '--device=plughw:1,0', msg_path])
    time.sleep(2)

    print 'Message Played'

def stop_playback():
    change_rings_stop(None)
    mp3_player_stop(None)
    wedding_sequence_stop(None)
    
    
def shutdown():                                         # If outside Opening Hours stop all the players
    if not opening_hours:
        print 'Shut down everything'
        stop_playback()

def scheduledws():                                      # Run the Scheduled Wedding Sequence
    if opening_hours:
        calculate_hours()
        if hour == 11:
            print '2 oclock'
            print 'Start the Scheduled Wedding Sequence'
            wedding_sequence_start(None)
        elif hour == 14:
            print '11 oclock'
            print 'Start the Scheduled Wedding Sequence'
            wedding_sequence_start(None)
        else:
            print 'Not time for Wedding Sequence'

def scheduledmp3():                                      # Run the Scheduled MP3 Player (10:04 am)
    global playlist_index
    if opening_hours:
        calculate_hours()
        if hour == 10:
            print 'Six Minutes past 10 oclock in the morning'
            print 'Start the Scheduled MP3 Player using the same Playlist as when stopped'
            if playlist_index > 1:
                playlist_index -= 1                         # Select the Playlist before the current one.
                mp3_player_start(None)                      # (mp3_player_start() increments the index.)
            print 'MP3 Player wasn''t running when system was disabled' 
        else:
            print 'Not hour for scheduled MP3 Player'

def change_rings_start(channel):                         # Play a file of change rings
    global c_player

    stop_playback()
    
    print 'Change rings stopped'
    
    c_file = 'Wimborne_Minster_StedmanCinques_16-10-2016.mp3' # Change rings.
    c_path = os.path.join(chr_subdir, c_file)
    blackhole = open(os.devnull, 'w')
    
    if c_player is None:
        c_player = subprocess.Popen(['mpg321', '-m', '-a', 'hw:0,0', c_path], stdin=subprocess.PIPE, stdout=blackhole, stderr=blackhole)

    print 'Playing Changes - ', c_file, 'to the Tower'

def change_rings_stop(channel):                         # Play a file of change rings
    global c_player


    if c_player is None:
        return

    c_player.terminate()
    c_wait = c_player.wait()
    if not c_wait:
        print 'Change Rings: mpg321 failed:%#x\n' % c_wait
    c_player = None

def mp3_player_start(channel):                          #  Setup and run the MP3 Player
    global playlist_index, mp3_player

    stop_playback()

    playlist_index += 1                                 # Select the next Playlist.
    playlist_index %= len(playlists)                    # Cycle round again after 6.
    print playlist_index
    
    mp3_subdir = playlists[playlist_index - 1]          # directory containing mp3 files for the current Playlist
    
    playlist = sorted(glob.glob(mp3_subdir + '/*.[Mm][Pp]3'))   # Create a list of files to be played in the current directory

    mpg_list = ['mpg321', '-l', '-m', '-a', 'hw:1,0']

    args = mpg_list + playlist
    print args

    blackhole = open(os.devnull, 'w')
    
    if mp3_player is None:
        mp3_player = subprocess.Popen(args)

    print 'Playing to the Chancel'

def mp3_player_stop(channel):                           #  Setup and run the MP3 Player
    global mp3_player

    if mp3_player is None:
        return

    mp3_player.terminate()
    mp3_wait = mp3_player.wait()
    if not mp3_wait:
        print 'MP3 Player: mpg321 failed:%#x\n' % mp3_wait
    mp3_player = None
        
def wedding_sequence_start(channel):                    # Play a wedding ceremony and change rings
    global wtr_player, wch_player, ws_timer

    stop_playback()

    time.sleep(5)

    ws_playlist = sorted(glob.glob(ws_subdir + '/*.[Mm][Pp]3'))   # Create a list of files to be played in the current directory

    ws_mpg_list = ['mpg321', '-m', '-a', 'hw:1,0']

    ws_args = ws_mpg_list + ws_playlist

    blackhole = open(os.devnull, 'w')
    
    if wch_player is None:
        wch_player = subprocess.Popen(ws_args)

    print 'Playing Wedding Sequence to the Chancel'

    wb_playlist = sorted(glob.glob(wb_subdir + '/*.[Mm][Pp]3'))   # Create a list of files to be played in the current directory

    wb_mpg_list = ['mpg321', '-m', '-a', 'hw:0,0']

    wb_args = wb_mpg_list + wb_playlist
    print wb_args

    blackhole = open(os.devnull, 'w')
    print wtr_player
    if wtr_player is None:
        wtr_player = subprocess.Popen(wb_args)

    print 'Playing Wedding bells to the Tower'

    ws_timer = sched.add_job(restartmp3, 'interval', minutes=12)    # Restart MP3 Player using a timer that will trigger after the Sequence is completed.

def wedding_sequence_stop(channel):                           #  Stop the Wedding Sequence
    global wtr_player, wch_player

    if wch_player == None and wtr_player == None:
        return

    if wch_player:
        wch_player.terminate()
        wch_wait = wch_player.wait()
        if not wch_wait:
            print 'Wedding Sequence in Chancel: mpg321 failed:%#x\n' % wch_wait
        wch_player = None
        time.sleep(2)                                   # Wait to give the mpg321 player time to start the bells
        
    if wtr_player:
        wtr_player.terminate()
        wtr_wait = wtr_player.wait()
        if not wtr_wait:
            print 'Wedding Sequence in Chancel: mpg321 failed:%#x\n' % wtr_wait
        wtr_player = None
        
    print 'Wedding Sequence Terminated'

def restartmp3():                           # Restart the MP3 Player if the Wedding Sequence is completed.
    global playlist_index
    
    playlist_index -= 1                     # Select the Playlist before the current one.
    mp3_player_start(None)                  # (mp3_player_start() increments the index.)
    ws_timer.remove()                       # Cancel the timer started during the Wedding Sequence.

def resetnormalopening():                   # Reset Opening Hours to normal after the site has closed for the night.
    global enable_late
    
    enable_late = False
    print 'Opening Hours reset to normal'

def chimes_schedule():                      # Schedule functions to be run and start the scheduler
    sched.add_job(hours, 'cron', hour='10')
    sched.add_job(hours, 'cron', hour='11')
    sched.add_job(hours, 'cron', hour='12')
    sched.add_job(hours, 'cron', hour='13')
    sched.add_job(hours, 'cron', hour='14')
    sched.add_job(hours, 'cron', hour='15')
    sched.add_job(hours, 'cron', hour='16')
    sched.add_job(hours, 'cron', hour='17')
    sched.add_job(hours, 'cron', hour='18')
    sched.add_job(hours, 'cron', hour='19')
    sched.add_job(hours, 'cron', hour='20')
    sched.add_job(hours, 'cron', hour='21')
    sched.add_job(hours, 'cron', hour='22')
    sched.add_job(resetnormalopening, 'cron', hour='23')
    sched.add_job(shutdown, 'cron', minute='1')
    sched.add_job(scheduledws, 'cron', minute='3')
    sched.add_job(quarters, 'cron', minute='15')
    sched.add_job(quarters, 'cron', minute='30')
    sched.add_job(quarters, 'cron', minute='45')
    sched.add_job(scheduledmp3, 'cron', minute='6')

    sched.start()

def add_switch_events():
    GPIO.add_event_detect(20, GPIO.RISING, callback=extended_hours_trigger,bouncetime=3000)
    GPIO.add_event_detect(21, GPIO.RISING, callback=extended_hours_disable,bouncetime=3000)
    GPIO.add_event_detect(22, GPIO.RISING, callback=change_rings_start,bouncetime=3000)
    GPIO.add_event_detect(23, GPIO.RISING, callback=change_rings_stop,bouncetime=3000)
    GPIO.add_event_detect(24, GPIO.RISING, callback=mp3_player_start,bouncetime=3000)
    GPIO.add_event_detect(25, GPIO.RISING, callback=mp3_player_stop,bouncetime=3000)
    GPIO.add_event_detect(26, GPIO.RISING, callback=wedding_sequence_start,bouncetime=3000)
    GPIO.add_event_detect(27, GPIO.RISING, callback=wedding_sequence_stop,bouncetime=3000)



################################################ Main Program Begins Here  ###########################################

try:
    chimes_schedule()

    set_volume()

    add_switch_events()

    check_silent_hours()

    if opening_hours:                       # Program has begun after 10 am, so start the music.
        mp3_player_start(None)

    while 1:                                # Otherwise, idle until Opening Hours is true and an event occurs.
        time.sleep(2**31-1)
    

except KeyboardInterrupt:
    GPIO.cleanup()
    
